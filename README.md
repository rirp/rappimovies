# Capas de la aplicacion
# Parquetes
-di: contiene todo lo necesario para inicializar una aplicación con Dagger.
-entities: contiene todas las clases POJO.
-network: tiene el modulo para resolver las dependencias para consumir un servicio y la definiciónn de los servicios a consumir.
- ui: contiene todo lo que es la capa de presentación utilizando MVP - Model View Presenter.

- View: Encargada de mostrar la información al ususario.
        Las clases con la terminación en View, fragment y Activity.
- Model: Encargada de consultar los datos ya sea de preferencias, servicios webs, base de           datos o cualquier fuente de datos.
        Interfaces terminadas en Model con su respectiva implementacion (terminada en               ModelImp)
- Presenter: Conecta a la vista y al modelo. Contiene lógica de negocio.
            Interfaces terminadas en Presenter con su respectiva implementacion (Terminada              en PresenterImp).

# Clases
- MainActivity: Contiene el TabLayout y un ViewPager d´ónde se muestran el listado de las peliculsa.
- MainView: contrato del MainActivity para comunicarse en el MVP.
- MainPresenter y MainPresenterImp: capa de presentación que lo que hace es recibir los eventos de la vista y comunicarse cone l modelo, consulta la lista de peliculas según la página (Las categorias).
- MainModel y MainModelImp: Consulta los datos de los servicios y se los envia al presentador usando la interfaz MainModelCallback, en el caso que se produzca un error al consumir los servicios intenta obtener la información de las preferencias, en caso de no ser posible devuelve un mensaje de error.
- Constants: Contiene todas ls constantes utilizadas en la App.
- BaseActivity: Es la clase Base o padre que contiene todo los comportamientos básicos de las actividades.
- BaseModule: es una interfaz que su funcionalidad es que todos los modulos de dagger extiendan o implementen a esta. Utiliza un generico para resolver el contexto de la actividad actual.
- Scope Fragment y Scorpe Activity: Define el tiempo de duración de la depencia inyectada con dagger. cada uno segun el ciclo de vida de una actividad o un fragmento respectivamente.
- Activity Binding Module: están registradas las actividades a las cuales se le van a inyectar las dependencias.
- Detail y Main Activity module: resulven las dependencias para su respectiva actividad.
- ContentMovieFragment: Contiene el fragment que este a su vez contiene un recycler view para mostrar la listas de peliculas.
- Content Movie Fragment Module: resulve las dependencias de el fragmento.
- MovieFilter: clase ques e encarga de filtrar la lista de peliculas a travez del presentador. esta clase extiende de Filter.
- Movies Adapter: El adaptador del recycler view para mostrar la información.
- MovieViewHolder: Contenedor de cada elemento de la lista de peliculas. recibe un objeto Movie Entity que tiene la información individual de la pelicula.
- MovieResponseEntitiy: es el modelo base de como se reciben los datos del servicio.
- Main View Pager Adapter: Adaptador para resolver las páginas del View Pager.
# Preguntas
1. En qué consiste el principio de responsabilidad única? Cuál es su propósito? 
    Cada clase, modulo o interfaz debe tener una responsabilidad. Es decir, una única razón     por la que cambiar. Con el proposito de no mezclar funcionalidades.
2. Qué características tiene, según su opinión, un “buen” código o código limpio?
    - Una buena nemotécmia
    - No debe contener código duplicado.
    - Respetar los principios de SOLID.

