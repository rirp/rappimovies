package com.rappi.rappimovies.ui.main.view.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.rappi.rappimovies.R;
import com.rappi.rappimovies.entities.MovieEntity;

import java.util.Collections;
import java.util.List;

/**
 * Created by ronaldruiz on 7/4/18.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MovieViewHolder> implements Filterable{
    private List<MovieEntity> movieList;
    private List<MovieEntity> copie;
    private MovieFilter movieFilter;

    public void setMovieList(List<MovieEntity> movieList) {
        this.movieList = movieList;
        this.copie = movieList;
    }

    public void setCopies(List<MovieEntity> copie) {
        this.copie = copie;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.setData(copie.get(position));
    }

    @Override
    public int getItemCount() {
        if (copie == null) {
            return Collections.emptyList().size();
        }
        return copie.size();
    }

    @Override
    public Filter getFilter() {
        if (movieFilter == null) {
            movieFilter = new MovieFilter(movieList, this);
        }
        return movieFilter;
    }
}
