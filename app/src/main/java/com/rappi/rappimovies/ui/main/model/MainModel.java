package com.rappi.rappimovies.ui.main.model;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public interface MainModel {
    void getPopularMovies(MainModelCallback callback);
    void getUpcomingMovies(MainModelCallback callback);
    void getTopRatedMovies(MainModelCallback callback);
}
