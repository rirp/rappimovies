package com.rappi.rappimovies.ui.main.presenter;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public interface MainPresenter {
    void getMovieByCategory(int category);
}
