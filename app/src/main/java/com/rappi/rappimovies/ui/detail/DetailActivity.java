package com.rappi.rappimovies.ui.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.rappi.rappimovies.R;
import com.rappi.rappimovies.entities.MovieEntity;
import com.rappi.rappimovies.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity {
    @BindView(R.id.ig_poster)
    protected ImageView igPoster;
    @BindView(R.id.tv_description)
    protected TextView tvDescription;
    @BindView(R.id.tv_original_title)
    protected TextView tvOriginalTitle;
    @BindView(R.id.tv_detail_title)
    protected TextView tvDetailTitle;
    @BindView(R.id.tv_original_language)
    protected TextView tvOriginalLanguage;
    @BindView(R.id.tv_release_date)
    protected TextView tvReleaseDate;
    @BindView(R.id.tv_age)
    protected TextView tvAge;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        MovieEntity movieEntity = (MovieEntity) getIntent().getSerializableExtra("movie");

        tvDescription.setText(movieEntity.getOverview());
        tvOriginalLanguage.setText(movieEntity.getOriginalLenguage());
        tvAge.setText(movieEntity.isAdult()? "+18" : "Everyone");
        tvOriginalTitle.setText(movieEntity.getOriginalTitle());
        tvDetailTitle.setText(movieEntity.getTitle());
        tvReleaseDate.setText(movieEntity.getReleaseDate());
    }
}
