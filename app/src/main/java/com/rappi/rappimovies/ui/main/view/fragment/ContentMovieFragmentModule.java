package com.rappi.rappimovies.ui.main.view.fragment;

import com.rappi.rappimovies.di.scopes.ScopeFragment;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 7/5/18.
 */
@Module
public abstract class ContentMovieFragmentModule {
    @ScopeFragment
    @Binds
    abstract ContentMovieView contentMovieViewInject(ContentMovieFragment fragment);

    @ScopeFragment
    @Provides
    static MoviesAdapter moviesAdapterInject() {
        return new MoviesAdapter();
    }
}
