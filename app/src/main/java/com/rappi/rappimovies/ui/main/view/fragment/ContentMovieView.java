package com.rappi.rappimovies.ui.main.view.fragment;

import com.rappi.rappimovies.entities.MovieEntity;

import java.util.List;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public interface ContentMovieView {
    void setMovies(List<MovieEntity> movieList);
}
