package com.rappi.rappimovies.ui.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.rappi.rappimovies.Constants;
import com.rappi.rappimovies.R;
import com.rappi.rappimovies.entities.MovieEntity;
import com.rappi.rappimovies.ui.base.BaseActivity;
import com.rappi.rappimovies.ui.main.presenter.MainPresenter;
import com.rappi.rappimovies.ui.main.view.fragment.ContentMovieFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements ViewPager.OnPageChangeListener, MainView {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.tb_categories)
    protected TabLayout tbCategories;
    @BindView(R.id.vp_list)
    protected ViewPager vpList;
    @Inject
    protected MainViewPagerAdapter pagerAdapter;
    @Inject
    protected MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initControllers();
    }

    private void initControllers() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        vpList.setAdapter(pagerAdapter);
        tbCategories.setupWithViewPager(vpList);
        vpList.addOnPageChangeListener(this);
        presenter.getMovieByCategory(Constants.MovieCategory.POPULAR);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        showLoading();
        presenter.getMovieByCategory(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onMovieList(List<MovieEntity> movieList) {
        ContentMovieFragment contentMovieFragment = (ContentMovieFragment) pagerAdapter.instantiateItem(vpList, vpList.getCurrentItem());
        contentMovieFragment.setMovies(movieList);
        removeLoading();
    }

    @Override
    public void showError(String message) {
        removeLoading();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
