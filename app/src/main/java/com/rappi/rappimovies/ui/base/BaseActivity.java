package com.rappi.rappimovies.ui.base;

import android.app.ProgressDialog;

import com.rappi.rappimovies.R;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by ronaldruiz on 7/3/18.
 */

public class BaseActivity extends DaggerAppCompatActivity {
    private ProgressDialog mLoadingDialog;

    public void showLoading() {
        if (this.mLoadingDialog == null || !this.mLoadingDialog.isShowing()) {
            this.mLoadingDialog = ProgressDialog
                    .show(this,
                            "",
                            this.getString(R.string.string_loading),
                            true,
                            false,
                            null);
        }
    }

    public void removeLoading() {
        try {
            if (this.mLoadingDialog != null && this.mLoadingDialog.isShowing()) {
                this.mLoadingDialog.dismiss();
                this.mLoadingDialog = null;
            }
        } catch (final IllegalArgumentException e) {
            this.mLoadingDialog = null;
        }
    }
}
