package com.rappi.rappimovies.ui.main.presenter;

import com.rappi.rappimovies.Constants;
import com.rappi.rappimovies.entities.MovieEntity;
import com.rappi.rappimovies.ui.main.model.MainModel;
import com.rappi.rappimovies.ui.main.model.MainModelCallback;
import com.rappi.rappimovies.ui.main.view.MainView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public class MainPresenterImp implements MainPresenter, MainModelCallback {
    private MainModel mModel;
    private MainView mView;

    @Inject
    public MainPresenterImp(MainModel mModel, MainView mView) {
        this.mModel = mModel;
        this.mView = mView;
    }

    @Override
    public void getMovieByCategory(int category) {
        switch (category) {
            case Constants.MovieCategory.POPULAR:
                mModel.getPopularMovies(this);
                break;
            case Constants.MovieCategory.UPCOMING:
                mModel.getUpcomingMovies(this);
                break;
            default:
                mModel.getTopRatedMovies(this);
                break;
        }
    }

    @Override
    public void onMovie(List<MovieEntity> movieList) {
        mView.onMovieList(movieList);
    }

    @Override
    public void showError(String message) {
        mView.showError(message);
    }
}
