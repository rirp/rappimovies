package com.rappi.rappimovies.ui.main.view;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.rappi.rappimovies.ui.main.view.fragment.ContentMovieFragment;

import java.util.List;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {
    private List<String> mTitles;
    private static final int NUMBER_PAGE = 3;

    public MainViewPagerAdapter(FragmentManager manager, List<String> titles) {
        super(manager);
        mTitles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return ContentMovieFragment.newInstance();
    }

    @Override
    public int getCount() {
        return NUMBER_PAGE;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }
}
