package com.rappi.rappimovies.ui.main.model;

import android.content.SharedPreferences;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.rappi.rappimovies.Constants;
import com.rappi.rappimovies.entities.MovieEntity;
import com.rappi.rappimovies.entities.MovieResponseEntity;
import com.rappi.rappimovies.network.NetworkService;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public class MainModelImp implements MainModel {
    private Retrofit mRetrofit;
    private MainModelCallback mCallback;
    private SharedPreferences mSharedPreferences;

    public MainModelImp(Retrofit mRetrofit, SharedPreferences sharedPreferences) {
        this.mRetrofit = mRetrofit;
        this.mSharedPreferences = sharedPreferences;
    }

    @Override
    public void getPopularMovies(MainModelCallback callback) {
        mCallback = callback;
        getPopularObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver(Constants.MovieCategory.POPULAR_STRING));
    }

    @Override
    public void getUpcomingMovies(MainModelCallback callback) {
        mCallback = callback;

        getUpcomingObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver(Constants.MovieCategory.UPCOMING_STRING));
    }

    @Override
    public void getTopRatedMovies(MainModelCallback callback) {
        mCallback = callback;

        getTopRatedObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver(Constants.MovieCategory.TOP_RATED_STRING));
    }

    private Observable<MovieResponseEntity> getPopularObservable() {
        return mRetrofit.create(NetworkService.class).getPopularMovie(Constants.Api.API_KEY);
    }

    private Observable<MovieResponseEntity> getTopRatedObservable() {
        return mRetrofit.create(NetworkService.class).getTopRateMovie(Constants.Api.API_KEY);
    }

    private Observable<MovieResponseEntity> getUpcomingObservable() {
        return mRetrofit.create(NetworkService.class).getUpcomingMovie(Constants.Api.API_KEY);
    }

    private DisposableObserver<MovieResponseEntity> getObserver(final String category) {
        return new DisposableObserver<MovieResponseEntity>() {
            @Override
            public void onNext(MovieResponseEntity responseEntity) {
                mCallback.onMovie(responseEntity.getResults());
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putString(category, new Gson().toJson(responseEntity.getResults()));
                editor.apply();
            }

            @Override
            public void onError(Throwable e) {
                String json = mSharedPreferences.getString(category, "");
                if (!json.equals("")) {
                    Type type = new TypeToken<List<MovieEntity>>(){}.getType();
                    List<MovieEntity> list = new Gson().fromJson(json, type);
                    mCallback.onMovie(list);
                } else {
                    mCallback.showError("Error al cargar las peliculas, intente más tarde.");
                }
            }

            @Override
            public void onComplete() {

            }
        };
    }
}
