package com.rappi.rappimovies.ui.main.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rappi.rappimovies.BuildConfig;
import com.rappi.rappimovies.Constants;
import com.rappi.rappimovies.R;
import com.rappi.rappimovies.entities.MovieEntity;
import com.rappi.rappimovies.ui.detail.DetailActivity;

/**
 * Created by ronaldruiz on 7/4/18.
 */

public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView tvAdult;
    private TextView tvTitle;
    private TextView tvDescription;
    private ImageView imageView;
    private MovieEntity movieEntity;
    private Context mContext;

    public MovieViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        tvAdult = itemView.findViewById(R.id.tv_adult);
        tvDescription = itemView.findViewById(R.id.tv_description);
        tvTitle = itemView.findViewById(R.id.tv_title);
        imageView = itemView.findViewById(R.id.imageView);
        itemView.setOnClickListener(this);
    }

    public void setData(MovieEntity movie) {
        movieEntity = movie;
        tvTitle.setText(movie.getTitle());
        tvDescription.setText(movie.getOverview());
        tvAdult.setVisibility(movie.isAdult() ? View.VISIBLE : View.INVISIBLE);
        if (movie.getPosterPath() != null) {
            String poster = BuildConfig.BASE_URL
                    + movie.getPosterPath().substring(1)
                    + "?api_key="
                    + Constants.Api.API_KEY;
            imageView.setImageURI(Uri.parse(poster));
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(mContext, DetailActivity.class);
        intent.putExtra("movie", movieEntity);
        mContext.startActivity(intent);
    }
}
