package com.rappi.rappimovies.ui.detail;

import com.rappi.rappimovies.ui.base.BaseModule;

import dagger.Module;

@Module
public interface DetailActivityModule extends BaseModule<DetailActivity> {
}
