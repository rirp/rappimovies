package com.rappi.rappimovies.ui.base;

import android.support.v7.app.AppCompatActivity;

import com.rappi.rappimovies.di.scopes.ScopeActivity;

import dagger.Binds;
import dagger.Module;

@Module
public interface BaseModule<T extends AppCompatActivity> {
    @Binds
    @ScopeActivity
    AppCompatActivity activity(T activity);
}
