package com.rappi.rappimovies.ui.main.model;

import com.rappi.rappimovies.entities.MovieEntity;

import java.util.List;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public interface MainModelCallback {
    void onMovie(List<MovieEntity> movieList);
    void showError(String message);
}
