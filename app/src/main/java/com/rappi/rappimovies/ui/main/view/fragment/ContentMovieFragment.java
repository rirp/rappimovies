package com.rappi.rappimovies.ui.main.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.rappi.rappimovies.R;
import com.rappi.rappimovies.entities.MovieEntity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public class ContentMovieFragment extends DaggerFragment implements ContentMovieView, SearchView.OnQueryTextListener {
    @BindView(R.id.rv_movies)
    RecyclerView rvMovies;
    Unbinder unbinder;
    @Inject
    MoviesAdapter mAdapter;
    @BindView(R.id.search_view)
    SearchView searchView;

    public static ContentMovieFragment newInstance() {
        return new ContentMovieFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_movies, container, false);
        unbinder = ButterKnife.bind(this, view);
        rvMovies.setAdapter(mAdapter);
        rvMovies.setLayoutManager(new LinearLayoutManager(container.getContext()));
        searchView.setOnQueryTextListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setMovies(List<MovieEntity> movieList) {
        mAdapter.setMovieList(movieList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        mAdapter.getFilter().filter(s);
        return false;
    }
}
