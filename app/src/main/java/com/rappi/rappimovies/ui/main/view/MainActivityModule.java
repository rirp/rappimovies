package com.rappi.rappimovies.ui.main.view;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.rappi.rappimovies.R;
import com.rappi.rappimovies.di.scopes.ScopeActivity;
import com.rappi.rappimovies.di.scopes.ScopeFragment;
import com.rappi.rappimovies.ui.base.BaseModule;
import com.rappi.rappimovies.ui.main.model.MainModel;
import com.rappi.rappimovies.ui.main.model.MainModelImp;
import com.rappi.rappimovies.ui.main.presenter.MainPresenter;
import com.rappi.rappimovies.ui.main.presenter.MainPresenterImp;
import com.rappi.rappimovies.ui.main.view.fragment.ContentMovieFragment;
import com.rappi.rappimovies.ui.main.view.fragment.ContentMovieFragmentModule;

import java.util.Arrays;
import java.util.List;

import javax.inject.Scope;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 7/3/18.
 */
@Module
public abstract class MainActivityModule implements BaseModule<MainActivity> {
    @ScopeFragment
    @ContributesAndroidInjector(modules = ContentMovieFragmentModule.class)
    abstract ContentMovieFragment contentMovieFragmentInject();

    @Provides
    static MainViewPagerAdapter mainViewPagerAdapterInject(AppCompatActivity context) {
        List<String> titles = Arrays.asList(context.getResources().getStringArray(R.array.array_categories));
        return new MainViewPagerAdapter(context.getSupportFragmentManager(), titles);
    }

    @ScopeActivity
    @Binds
    abstract MainView mainViewInject(MainActivity mainActivity);

    @ScopeActivity
    @Binds
    abstract MainPresenter mainPresenterInject(MainPresenterImp presenterImp);

    @ScopeActivity
    @Provides
    static MainModel mainModelInject(Retrofit retrofit, SharedPreferences sharedPreferences) {
        return new MainModelImp(retrofit, sharedPreferences);
    }
}
