package com.rappi.rappimovies.ui.main.view;

import com.rappi.rappimovies.entities.MovieEntity;

import java.util.List;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public interface MainView {
    void onMovieList(List<MovieEntity> movieList);
    void showError(String message);
}
