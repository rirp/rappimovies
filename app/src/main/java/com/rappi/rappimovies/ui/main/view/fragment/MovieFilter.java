package com.rappi.rappimovies.ui.main.view.fragment;

import android.widget.Filter;

import com.rappi.rappimovies.entities.MovieEntity;

import java.util.ArrayList;
import java.util.List;

public class MovieFilter extends Filter {

    private List<MovieEntity> movies;
    private MoviesAdapter adapter;

    public MovieFilter(List<MovieEntity> movies, MoviesAdapter adapter) {
        this.movies = movies;
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        String charString = charSequence.toString();
        FilterResults results = new FilterResults();
        if (charString.isEmpty()) {
            results.count = movies.size();
            results.values = movies;
        } else {
            List<MovieEntity> moviesFilted = new ArrayList<>();
            for (MovieEntity movieEntity: movies) {
                if (movieEntity.getTitle().toUpperCase().contains(charString.toUpperCase())) {
                    moviesFilted.add(movieEntity);
                }
            }
            results.count = moviesFilted.size();
            results.values = moviesFilted;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        adapter.setCopies((List<MovieEntity>) filterResults.values);
        adapter.notifyDataSetChanged();
    }
}
