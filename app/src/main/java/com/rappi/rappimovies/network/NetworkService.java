package com.rappi.rappimovies.network;

import com.rappi.rappimovies.entities.MovieEntity;
import com.rappi.rappimovies.entities.MovieResponseEntity;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Network service contract
 * @author ronaldruiz on 5/14/18.
 */

public interface NetworkService {
    @GET("movie/popular")
    Observable<MovieResponseEntity> getPopularMovie(@Query("api_key") String apiKey);
    @GET("movie/top_rated")
    Observable<MovieResponseEntity> getTopRateMovie(@Query("api_key") String apiKey);
    @GET("movie/upcoming")
    Observable<MovieResponseEntity> getUpcomingMovie(@Query("api_key") String apiKey);
}
