package com.rappi.rappimovies.network;

import android.app.Application;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.rappi.rappimovies.BuildConfig;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ronaldruiz on 5/11/18.
 */
@Module
public class NetworkModule {
    private static final int TIME_OUT = 30;
    @Provides
    protected static OkHttpClient provideHttpClient(Cache cache){
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        client.readTimeout(TIME_OUT, TimeUnit.SECONDS);
        client.writeTimeout(TIME_OUT, TimeUnit.SECONDS);
        client.cache(cache);
        return client.build();
    }

    @Provides
    protected static Cache provideCache(Application application){
        int cacheSize = 1024*1024*10;
        return new Cache(application.getCacheDir(),cacheSize);
    }
    @Provides
    protected static Gson provideGson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    protected static Retrofit provideRetrofit(Gson gson, OkHttpClient client){
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }
}
