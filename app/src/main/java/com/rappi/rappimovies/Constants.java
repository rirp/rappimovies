package com.rappi.rappimovies;

/**
 * Created by ronaldruiz on 7/5/18.
 */

public interface Constants {
    class Api {
        public static final String API_KEY = "12956a722c68d23f60961096eaebdd15";
    }

    class MovieCategory {
        public static final int POPULAR             = 0;
        public static final int UPCOMING            = 1;
        public static final int TOP_RATED           = 2;
        public static final String POPULAR_STRING   = "popular";
        public static final String UPCOMING_STRING  = "upcoming";
        public static final String TOP_RATED_STRING = "topRated";
    }
}
