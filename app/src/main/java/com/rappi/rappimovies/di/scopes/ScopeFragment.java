package com.rappi.rappimovies.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ronaldruiz on 5/9/18.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ScopeFragment {
}
