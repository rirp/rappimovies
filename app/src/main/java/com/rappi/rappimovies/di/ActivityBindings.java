package com.rappi.rappimovies.di;

import com.rappi.rappimovies.ui.detail.DetailActivity;
import com.rappi.rappimovies.ui.detail.DetailActivityModule;
import com.rappi.rappimovies.ui.main.view.MainActivity;
import com.rappi.rappimovies.di.scopes.ScopeActivity;
import com.rappi.rappimovies.ui.main.view.MainActivityModule;

import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ronaldruiz on 6/25/18.
 */

@Module(includes = AndroidInjectionModule.class)
public interface ActivityBindings {
    @ScopeActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    MainActivity mainActivityInjector();

    @ScopeActivity
    @ContributesAndroidInjector(modules = DetailActivityModule.class)
    DetailActivity detailActivityInjector();
}
