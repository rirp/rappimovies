package com.rappi.rappimovies.di.app;

import android.app.Application;

import com.rappi.rappimovies.di.ActivityBindings;
import com.rappi.rappimovies.network.NetworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by ronaldruiz on 7/3/18.
 */
@Singleton
@Component(modules = {AppModule.class,
        AndroidSupportInjectionModule.class,
        ActivityBindings.class,
        NetworkModule.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {
    void inject(App app);

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
