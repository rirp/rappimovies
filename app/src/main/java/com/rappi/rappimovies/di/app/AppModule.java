package com.rappi.rappimovies.di.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.rappi.rappimovies.R;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;

/**
 * Created by ronaldruiz on 7/3/18.
 */
@Module(includes = AndroidInjectionModule.class)
public class AppModule {
    @Singleton
    @Provides
    protected static SharedPreferences sharedPreferencesProvide(Application application) {
        return application.getSharedPreferences(application.getString(R.string.app_name), Context.MODE_PRIVATE);
    }
}
